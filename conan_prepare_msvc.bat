set CONAN_USER_HOME_SHORT=None

conan install . --install-folder .conandeps --build=missing -g visual_studio_multi -s arch=x86 -s build_type=Debug
conan install . --install-folder .conandeps --build=missing -g visual_studio_multi -s arch=x86_64 -s build_type=Debug
conan install . --install-folder .conandeps --build=missing -g visual_studio_multi -s arch=x86 -s build_type=Release
conan install . --install-folder .conandeps --build=missing -g visual_studio_multi -s arch=x86_64 -s build_type=Release
