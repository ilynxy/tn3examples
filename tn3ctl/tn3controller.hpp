#ifndef TN3CONTROLLER_HPP
#define TN3CONTROLLER_HPP

#include "udpsocket.hpp"
#include "tn3proto.hpp"

namespace tn3
{
  using ::udp::socket;
  using ::udp::endpoint_t;
  using ::udp::timeoutms_t;
  using ::udp::broadcast;

  using namespace tn3::proto::udp;
  using namespace tn3::proto;

  struct device_handle
  {
    endpoint_t  ep;
    lun_t       lun;
    CSW_PARAMS  extra;
  };

  class controller
  {
    socket  socket_;
    reqid_t reqid_;

    reqid_t next_request_id() noexcept
    {
      return reqid_++;
    }

    static seqcnt_t next_sequence_counter(seqcnt_t sc) noexcept
    {
      return (sc + 1);
    }

    static void prepare_proto(PROTO_HEADER& proto, signature_t signature, lun_t lun, reqid_t reqid, seqcnt_t seqcnt)
    {
      proto.signature = signature;
      proto.lun = lun;
      proto.reqid = reqid;
      proto.seqcnt = seqcnt;
    }

    static status_t validate_proto(const PROTO_HEADER *proto,
      size_t signature, lun_t lun, reqid_t reqid, seqcnt_t seqcnt)
    {
      if (proto->signature != signature)
        return error::invalid_wrapper_signature;

      if ((proto->lun != lun && lun != LUN_ANY) || proto->lun == LUN_ANY)
        return error::invalid_lun;

      if (proto->reqid != reqid)
        return error::invalid_sequence_id;

      if (proto->seqcnt != seqcnt)
        return error::invalid_sequence_counter;

      return error::success;
    }

    static status_t validate_CSW(const void *p, size_t received,
      lun_t lun, reqid_t reqid, seqcnt_t seqcnt, reqsize_t transfer_size)
    {
      if (received != COMMAND_STATUS_WRAPPER_SIZE)
        return error::invalid_wrapper_size;

      const CSW *pCSW = reinterpret_cast<const CSW *>(p);

      status_t status = validate_proto(&pCSW->proto, COMMAND_STATUS_WRAPPER_SIGNATURE, lun, reqid, seqcnt);
      if (status != error::success)
        return status;

      if (pCSW->dataTransferLength > transfer_size)
        return error::invalid_transfer_size;

      return pCSW->statusCode;
    }

    static status_t validate_DSW(const void *p, size_t received,
      lun_t lun, reqid_t reqid, seqcnt_t seqcnt, reqsize_t transfer_size)
    {
      if (received != DATA_STATUS_WRAPPER_SIZE)
        return error::invalid_wrapper_size;

      const DSW *pDSW = reinterpret_cast<const DSW *>(p);

      status_t status = validate_proto(&pDSW->proto, DATA_STATUS_WRAPPER_SIGNATURE, lun, reqid, seqcnt);
      if (status != error::success)
        return status;

      if (pDSW->dataTransferLength != transfer_size)
        return error::invalid_transfer_size;

      return pDSW->statusCode;
    }

    static status_t validate_DBW(const void *p, size_t received,
      lun_t lun, reqid_t reqid, seqcnt_t seqcnt, reqsize_t transfer_size)
    {
      if (received < DATA_BLOCK_WRAPPER_SIZE)
        return error::invalid_wrapper_size;

      const DBW *pDBW = reinterpret_cast<const DBW *>(p);

      status_t status = validate_proto(&pDBW->proto, DATA_BLOCK_WRAPPER_SIGNATURE, lun, reqid, seqcnt);
      if (status != error::success)
        return status;

      received -= DATA_BLOCK_WRAPPER_SIZE;

      if (pDBW->dataTransferLength > received)
        return error::invalid_dbw_size;

      if (pDBW->dataTransferLength != transfer_size)
        return error::invalid_transfer_size;

      return pDBW->statusCode;
    }

  public:
    status_t bind_ep(endpoint_t& ep)
    {
      return socket_.bind(ep);
    }

    status_t enumerate(std::vector<device_handle> &handles, timeoutms_t timeoutms = 1000, std::size_t max_devices = -1)
    {
      status_t status = 0;
      handles.clear();

      do
      {
        reqid_t   reqid = next_request_id();
        seqcnt_t  seqcnt = 0;
        lun_t     lun = LUN_ANY;

        endpoint_t sep(broadcast, DEFAULT_PORT); // broadcast address

        // Prepare CBW structure
        CBW cbw;
        prepare_proto(cbw.proto, COMMAND_BLOCK_WRAPPER_SIGNATURE, lun, reqid, seqcnt);

        cbw.dataTransferLength = 0x00000000;
        cbw.requestCode = request::PING;
        cbw.flags = 0x00;

        // Send CBW in broadcast manner
        status = socket_.sendto(sep, &cbw, sizeof(cbw));
        if (status != 0)
          break;

        // CSW must contain increased sequence counter
        seqcnt = next_sequence_counter(seqcnt);

        auto start = std::chrono::steady_clock::now();
        for (;;)
        {
          if (handles.size() == max_devices)
            break;

          auto now = std::chrono::steady_clock::now();
          timeoutms_t residuems = static_cast<timeoutms_t>(std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count());
          if (residuems > timeoutms)
            break;

          // get replied CSW in buffer and catch reply endpoint
          endpoint_t   rep;
          size_t       received = 0;
          char         buff[65536];
          status = socket_.recvfrom(rep, buff, sizeof(buff), &received, residuems);
          if (status != 0 && status != boost::asio::error::timed_out)
            break;

          // Something looks like an PING reply?
          status = validate_CSW(buff, received, LUN_ANY, reqid, seqcnt, 0);
          if (status == 0)
          {
            const CSW *pCSW = reinterpret_cast<const CSW *>(buff);

            device_handle h;
            h.ep = rep;
            h.lun = pCSW->proto.lun;
            h.extra = pCSW->params;

            // store reply endpoint, lun and, may be, additional data
            // TODO: check duplicates and so on
            handles.push_back(h);
          }
        }

      } while (false);

      return status;
    }

  protected:
    status_t iorequest_generic(
      socket& s, const endpoint_t& ep, lun_t lun,
      request_t request, flags_t flags,
      void *data, reqsize_t transfer_size, reqsize_t *transferred = nullptr,
      const CBW_PARAMS *cbw_params = nullptr,
      CSW_PARAMS *csw_params = nullptr,
      timeoutms_t timeoutms = 1000)
    {
      status_t  status = 0;

      do
      {
        const size_t max_transfer_length = 1024;

        reqid_t   reqid = next_request_id();
        seqcnt_t  seqcnt = 0;

        CBW cbw;
        prepare_proto(cbw.proto, COMMAND_BLOCK_WRAPPER_SIGNATURE, lun, reqid, seqcnt);

        cbw.dataTransferLength = transfer_size;
        cbw.requestCode = request;
        cbw.flags = flags;

        if (cbw_params != nullptr)
          cbw.params = *cbw_params;

        status = s.sendto(ep, &cbw, sizeof(cbw));
        if (status != 0)
          break;
        seqcnt = next_sequence_counter(seqcnt);

        endpoint_t  rep;
        size_t                  received = 0;
        char buff[65536];
        status = s.recvfrom(rep, buff, sizeof(buff), &received, timeoutms);
        if (status != 0)
          break;

        status = validate_CSW(buff, received, lun, reqid, seqcnt, transfer_size);
        if (status != 0)
          break;
        seqcnt = next_sequence_counter(seqcnt);

        const CSW *pCSW = reinterpret_cast<const CSW *>(buff);

        transfer_size = pCSW->dataTransferLength;

        if (csw_params != nullptr)
          *csw_params = pCSW->params;

        if (transfer_size == 0)
          break;

        size_t transferred_ = 0;
        endpoint_t sep = rep;

        if (flags & 0x80) // device -> host
        {
          char  *pdst = reinterpret_cast<char *>(data);

          do
          {
            status = s.recvfrom(rep, buff, sizeof(buff), &received, timeoutms);
            if (status != 0)
              break;

            reqsize_t current_transfer = transfer_size;
            if (current_transfer > max_transfer_length)
              current_transfer = max_transfer_length;

            status = validate_DBW(buff, received, lun, reqid, seqcnt, current_transfer);
            if (status != 0)
              break;
            seqcnt = next_sequence_counter(seqcnt);

            // const TN3_UDP_DBW *pDBW = reinterpret_cast<const TN3_UDP_DBW *>(buff);
            const char        *psrc = buff + sizeof(DBW);

            std::uninitialized_copy_n(psrc, current_transfer, pdst);
            pdst += current_transfer;
            transfer_size -= current_transfer;
            transferred_ += current_transfer;

            DSW *pDSW = reinterpret_cast<DSW *>(buff);
            prepare_proto(pDSW->proto, DATA_STATUS_WRAPPER_SIGNATURE, lun, reqid, seqcnt);

            pDSW->dataTransferLength = transfer_size;
            pDSW->statusCode = 0;

            status = s.sendto(sep, buff, sizeof(DSW));
            if (status != 0)
              break;
            seqcnt = next_sequence_counter(seqcnt);

          } while (transfer_size != 0);
        }
        else // host -> device
        {
          const char  *psrc = reinterpret_cast<const char *>(data);

          do
          {
            DBW  *pDBW = reinterpret_cast<DBW *>(buff);
            char *pdst = buff + sizeof(DBW);

            reqsize_t current_transfer = transfer_size;
            if (current_transfer > max_transfer_length)
              current_transfer = max_transfer_length;

            std::uninitialized_copy_n(psrc, current_transfer, pdst);
            psrc += current_transfer;

            prepare_proto(pDBW->proto, DATA_BLOCK_WRAPPER_SIGNATURE, lun, reqid, seqcnt);

            pDBW->dataTransferLength = current_transfer;
            pDBW->statusCode = 0;

            status = s.sendto(sep, buff, sizeof(DBW) + current_transfer);
            if (status != 0)
              break;

            seqcnt = next_sequence_counter(seqcnt);
            transfer_size -= current_transfer;

            status = s.recvfrom(rep, buff, sizeof(buff), &received, timeoutms);
            if (status != 0)
              break;

            status = validate_DSW(buff, received, lun, reqid, seqcnt, transfer_size);
            if (status != 0)
              break;

            // const TN3_UDP_DSW *pDSW = reinterpret_cast<const TN3_UDP_DSW *>(buff);

            seqcnt = next_sequence_counter(seqcnt);

            transferred_ += current_transfer;

          } while (transfer_size != 0);

        }

        if (transferred != nullptr)
          *transferred = static_cast<reqsize_t>(transferred_);

      } while (false);


      return status;
    }

  public:
    status_t ping(const device_handle& dh)
    {
      auto status = iorequest_generic(
        socket_, dh.ep, dh.lun,
        request::PING,
        request::direction::NONE,
        nullptr, 0
      );

      return status;
    }

    status_t read_task_memory(const device_handle& dh, std::uint32_t offset, void *buffer, std::uint32_t size, std::size_t& transferred)
    {
      CBW_PARAMS cbw_params;

      cbw_params.u32[0] = offset;
      cbw_params.u32[1] = size;

      reqsize_t transferred_;
      auto status = iorequest_generic(
        socket_, dh.ep, dh.lun,
        request::READ_TASK_MEMORY,
        request::direction::DEVICE_TO_CONTROLLER,
        buffer, size,
        &transferred_,
        &cbw_params
      );

      transferred = transferred_;

      return status;
    }

    status_t read_modules_map_memory(const device_handle& dh, std::uint32_t offset, void *buffer, std::uint32_t size, std::size_t& transferred)
    {
      CBW_PARAMS cbw_params;

      cbw_params.u32[0] = offset;
      cbw_params.u32[1] = size;

      reqsize_t transferred_;
      auto status = iorequest_generic(
        socket_, dh.ep, dh.lun,
        request::READ_MODULES_MAP,
        request::direction::DEVICE_TO_CONTROLLER,
        buffer, size,
        &transferred_,
        &cbw_params
      );

      transferred = transferred_;

      return status;
    }

    status_t read_boot_eeprom(const device_handle& dh, std::uint32_t offset, void *buffer, std::uint32_t size, std::size_t& transferred)
    {
      CBW_PARAMS cbw_params;

      cbw_params.u32[0] = offset;
      cbw_params.u32[1] = size;

      reqsize_t transferred_;
      auto status = iorequest_generic(
        socket_, dh.ep, dh.lun,
        request::BOOT_EEPROM_RW,
        request::direction::DEVICE_TO_CONTROLLER,
        buffer, size,
        &transferred_,
        &cbw_params
      );

      transferred = transferred_;

      return status;
    }

    status_t write_boot_eeprom(const device_handle& dh, std::uint32_t offset, const void *buffer, std::uint32_t size, std::size_t& transferred)
    {
      CBW_PARAMS cbw_params;

      cbw_params.u32[0] = offset;
      cbw_params.u32[1] = size;

      reqsize_t transferred_;
      auto status = iorequest_generic(
        socket_, dh.ep, dh.lun,
        request::BOOT_EEPROM_RW,
        request::direction::CONTROLLER_TO_DEVICE,
        const_cast<void *>(buffer), size,
        &transferred_,
        &cbw_params
      );

      transferred = transferred_;

      return status;
    }


    status_t read_icb_buffer(const device_handle& h, std::uint8_t request, std::uint8_t icb_address, std::uint16_t length)
    {
      status_t status;

      return status;
    }

    status_t write_icb_buffer()
    {
      status_t status;
      return status;
    }

    status_t execute_icb_request()
    {
      status_t status;
      return status;
    }

  public:
    controller()
      : reqid_(0)
    {
    }

    ~controller()
    {
    }
  };
}

#endif // TN3CONTROLLER_HPP
