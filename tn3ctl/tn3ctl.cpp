#include "pch.hpp"

#include <memory>
#include <fmt/format.h>

#include "tn3controller.hpp"

namespace fmt
{
  template<>
  struct formatter<tn3::device_handle>
  {
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template<typename FormatContext>
    auto format(const tn3::device_handle& dev, FormatContext& ctx)
    {
      auto ip = dev.ep.address().to_string();
      auto port = dev.ep.port();
      auto version = dev.extra.u32[0];
      return format_to(ctx.begin(),
        "{}:{}:{}:{}.{:02d}",
        ip, port,
        dev.lun,
        version / 256, version % 256
      );
    }
  };
}

int main(int argc, char* argv[])
{

  tn3::controller ctl;
  tn3::status_t status;

  //auto bep = udp::endpoint_t(boost::asio::ip::address_v4::from_string("192.168.0.2"), 0);
  //auto status = ctl.bind_ep(bep);

  std::vector<tn3::device_handle> dh;
  status = ctl.enumerate(dh, 200);

  if (dh.size() == 0)
  {
    fmt::print("No TN3 devices found...\n");
    return -1;
  }

  for (std::size_t i = 0; i < dh.size(); ++ i)
    fmt::print("Device #{} = {}\n", i, dh[i]);

  auto& dev = dh[0];

  fmt::print("Using device #0 with IP:PORT:LUN:VER = {}\n", dev);

  constexpr std::uint32_t task_buffer_size = 16384;
  char task_buffer[task_buffer_size];
  std::size_t task_size;
  status = ctl.read_task_memory(dev, 0, task_buffer, task_buffer_size, task_size);
  if (status != 0)
  {
    fmt::print("read_task_memory() error: 0x{:08X}\n", status);
    return -1;
  }

  fmt::print("read_task_memory() read bytes: {}\n", task_size);

  constexpr std::uint32_t modules_map_buffer_size = 256;
  char modules_map_buffer[modules_map_buffer_size];
  std::size_t modules_map_size;
  status = ctl.read_modules_map_memory(dev, 0, modules_map_buffer, modules_map_buffer_size, modules_map_size);
  if (status != 0)
  {
    fmt::print("read_modules_map_memory() error: 0x{:08X}\n", status);
    return -1;
  }

  fmt::print("read_modules_map_memory() read bytes: {}\n", modules_map_size);

  return 0;
}
