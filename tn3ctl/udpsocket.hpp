#ifndef UDPSOCKET_HPP
#define UDPSOCKET_HPP

//#define BOOST_ASIO_DISABLE_IOCP
//#define BOOST_ASIO_ENABLE_HANDLER_TRACKING

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <chrono>

namespace udp
{

typedef boost::asio::ip::address_v4     address_t;
typedef boost::asio::ip::udp::endpoint  endpoint_t;
typedef int                             status_t;
typedef unsigned                        timeoutms_t;
// using namespace boost::asio::error;

const address_t broadcast(boost::asio::ip::address_v4::broadcast());

class socket
{

public:
  socket()
    : socket_(get_service())
    , timer_(get_service())
  {
    socket_.open(boost::asio::ip::udp::v4());
    socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
    socket_.set_option(boost::asio::ip::udp::socket::broadcast(true));
  }

  socket(const endpoint_t &bind_addr)
    : socket()
  {
    socket_.bind(bind_addr);
  }

  ~socket()
  {
  }

  status_t bind(const endpoint_t &bind_addr)
  {
    boost::system::error_code ec;
    socket_.bind(bind_addr, ec);
    return ec.value();
  }

  status_t sendto(
    const endpoint_t& endpoint,
    const void*       ptr,
    size_t            size,
    size_t*           sent = nullptr
    )
  {
    status_t status;

    std::size_t bytes_transferred;
    boost::system::error_code ec;

    bytes_transferred = socket_.send_to(
      boost::asio::buffer(ptr, size),
      endpoint,
      0,
      ec);

    status = ec.value();

    if (sent != nullptr)
      *sent = bytes_transferred;

    return status;
  }

  status_t recvfrom(endpoint_t& endpoint, void *ptr, size_t size, size_t *received = nullptr, timeoutms_t timeout = 2000)
  {
    using namespace boost::system;
    using namespace boost::asio;

    status_t status;
    error_code ec;

    error_code  timer_ec = error::would_block;
    error_code  recv_ec  = error::would_block;
    std::size_t bytes_transferred = 0;

    auto recv_fn = [&recv_ec, &bytes_transferred](const boost::system::error_code& ec, std::size_t transferred)
    {
      recv_ec = ec;
      bytes_transferred = transferred;
    };

    auto timer_fn = [&timer_ec](const boost::system::error_code& ec)
    {
      timer_ec = ec;
    };

    timer_.expires_from_now(std::chrono::milliseconds(timeout));
    timer_.async_wait(timer_fn);

    socket_.async_receive_from(boost::asio::buffer(ptr, size), endpoint, 0, recv_fn);

    do
    {
      socket_.get_io_service().run_one();
    } while (recv_ec == error::would_block && timer_ec == error::would_block);

    if (recv_ec == error::would_block)
      socket_.cancel(ec);

    if (timer_ec == error::would_block)
      timer_.cancel(ec);

    while (recv_ec == error::would_block || timer_ec == error::would_block)
    {
      socket_.get_io_service().run_one();
    }

    if (recv_ec == error::operation_aborted && timer_ec.value() == 0)
    {
      recv_ec = error::timed_out;
    }

    status = recv_ec.value();

    if (received != nullptr)
      *received = bytes_transferred;

    return status;
  }

protected:
  boost::asio::ip::udp::socket  socket_;
  boost::asio::steady_timer     timer_;

  static boost::asio::io_service& get_service()
  {
    static boost::asio::io_service        io_service_;
    static boost::asio::io_service::work  work_(io_service_);
    return io_service_;
  }
};

}
#endif // UDPSOCKET_HPP
