#include "pch.hpp"

#include "reverse_bit_order.hpp"

static constexpr unsigned char bit_reverse_table_u8[256] =
{
#define R2(n)     n,     n + 2*64,     n + 1*64,     n + 3*64
#define R4(n) R2(n), R2(n + 2*16), R2(n + 1*16), R2(n + 3*16)
#define R6(n) R4(n), R4(n + 2*4 ), R4(n + 1*4 ), R4(n + 3*4 )
        R6(0), R6(2), R6(1), R6(3)
};

void reverse_bit_order_inplace(void *dst, std::size_t size)
{
  unsigned char *p = reinterpret_cast<unsigned char *>(dst);
  unsigned char *pe = p + size;
  while (p < pe)
  {
    *p = bit_reverse_table_u8[*p];
    ++ p;
  }
}
