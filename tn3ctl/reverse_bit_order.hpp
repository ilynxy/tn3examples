#ifndef REVERSE_BIT_ORDER
#define REVERSE_BIT_ORDER

#include <cstddef>

void reverse_bit_order_inplace(void *dst, std::size_t size);

#endif // REVERSE_BIT_ORDER

