#ifndef TN3PROTO_HPP
#define TN3PROTO_HPP

#include <cstddef>
#include <boost/endian/arithmetic.hpp>

namespace tn3
{

typedef std::uint32_t status_t;

namespace proto
{
namespace error
{
  constexpr status_t success                    =  0;
  constexpr status_t invalid_packet_size        = -1;
  constexpr status_t invalid_wrapper_signature  = -1;
  constexpr status_t invalid_wrapper_size       = -1;
  constexpr status_t invalid_lun                = -1;
  constexpr status_t invalid_sequence_id        = -1;
  constexpr status_t invalid_sequence_counter   = -1;
  constexpr status_t invalid_transfer_size      = -1;
  constexpr status_t invalid_dbw_size           = -1;
}

namespace udp
{

typedef std::uint16_t signature_t;
typedef std::uint16_t lun_t;
typedef std::uint16_t reqid_t;
typedef std::uint16_t seqcnt_t;
typedef std::uint8_t  request_t;
typedef std::uint8_t  flags_t;
typedef std::uint32_t reqsize_t;

constexpr std::size_t COMMAND_BLOCK_WRAPPER_SIZE        = 64;
constexpr signature_t COMMAND_BLOCK_WRAPPER_SIGNATURE   = 0x4243;
constexpr std::size_t COMMAND_STATUS_WRAPPER_SIZE       = 64;
constexpr signature_t COMMAND_STATUS_WRAPPER_SIGNATURE  = 0x5343;
constexpr std::size_t DATA_BLOCK_WRAPPER_SIZE           = 32;
constexpr signature_t DATA_BLOCK_WRAPPER_SIGNATURE      = 0x4244;
constexpr std::size_t DATA_STATUS_WRAPPER_SIZE          = 32;
constexpr signature_t DATA_STATUS_WRAPPER_SIGNATURE     = 0x5344;
constexpr lun_t       LUN_ANY                           = 0x0000;
constexpr std::uint16_t DEFAULT_PORT                    = 4880;

namespace request
{

constexpr request_t   PING              = 0x00;
constexpr request_t   READ_TASK_MEMORY  = 0x01;
constexpr request_t   READ_MODULES_MAP  = 0x02;
constexpr request_t   BOOT_EEPROM_RW    = 0x18;

constexpr request_t   MIB_EXT_RWBUFFER  = 0x10;
constexpr request_t   MIB_EXT_EXECUTE   = 0x11;

  namespace direction
  {
    constexpr flags_t     NONE = 0x00;
    constexpr flags_t     CONTROLLER_TO_DEVICE = 0x00;
    constexpr flags_t     DEVICE_TO_CONTROLLER = 0x80;
  }
}

typedef boost::endian::little_uint8_t   le_uint8_t;
typedef boost::endian::little_uint16_t  le_uint16_t;
typedef boost::endian::little_uint32_t  le_uint32_t;

struct PROTO_HEADER
{
  le_uint16_t  signature;
  le_uint16_t  lun;
  le_uint16_t  reqid;
  le_uint16_t  seqcnt; // Little endian
};

struct CBW_PARAMS
{
  le_uint32_t  u32[12];
};

struct CSW_PARAMS
{
  le_uint32_t  u32[12];
};

struct CBW
{
  PROTO_HEADER  proto;

  le_uint32_t   dataTransferLength; // Expected data transfer size by host

  le_uint8_t    requestCode;
  le_uint8_t    flags;  // 7    -- data stage direction (0 - host->device, 1 - device->host)
                        // 6    -- use BROADCAST_MAC:BROADCAST_IP:SENDER_PORT as target endpoint instead of SENDER_MAC:SENDER_IP:SENDER_PORT
                        // 5..0 -- currently reserved
  le_uint16_t   reserved;

  CBW_PARAMS    params;
};

static_assert(sizeof(CBW) == COMMAND_BLOCK_WRAPPER_SIZE);

struct CSW
{
  PROTO_HEADER  proto;

  le_uint32_t   dataTransferLength; // Exact data transfer size by device
  le_uint32_t   statusCode;

  CSW_PARAMS    params;
};

static_assert(sizeof(CSW) == COMMAND_STATUS_WRAPPER_SIZE);

struct DBW
{
  PROTO_HEADER  proto;

  //  dataTransferLength
  //      valid data size in current packet (packet size can
  //      be greater than valid data size, but in most cases
  //      must be equal, except tail data)
  le_uint32_t   dataTransferLength; // Exact data transfer size by device

  // data processing result or protocol handling status
  // (differentiated by code)
  // data stage transfer must be canceled if status is not successful
  le_uint32_t   statusCode;

  le_uint32_t   params[4];
};

static_assert(sizeof(DBW) == DATA_BLOCK_WRAPPER_SIZE);

struct DSW
{
  PROTO_HEADER  proto;

  //  dataTransferLength
  //      ignored (for debug purposes can be residue transfer length)
  le_uint32_t   dataTransferLength;

  // data processing result or protocol handling status
  // (differentiated by code)
  // data stage transfer must be canceled if status is not successful
  le_uint32_t   statusCode;

  le_uint32_t   params[4];
};

static_assert(sizeof(DSW) == DATA_STATUS_WRAPPER_SIZE);

} // namespace udp
} // namespace proto
} // namespace tn3

#endif // TN3PROTO_HPP
