// tn3eeprom.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.hpp"

#include <fmt/format.h>
#include <string_view>
#include <filesystem>
#include <charconv>
#include <fstream>
#include <memory>
#include <chrono>

#include "tn3controller.hpp"
#include "reverse_bit_order.hpp"

struct options
{
};

class progress
{
  std::size_t current_;
  std::size_t total_;

  using time_point_t = std::chrono::time_point<std::chrono::steady_clock>;

  time_point_t  start_;
  time_point_t  end_;
public:
  progress(std::size_t total)
    : current_(0)
    , total_(total)
    , start_(std::chrono::steady_clock::now())
    , end_(start_)
  {
  }

  void advance(std::size_t step)
  {
    current_ += step;
  }

  void print(std::string_view prefix, std::size_t interval_milliseconds = 250)
  {
    auto now = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - end_).count();
    if (elapsed >= interval_milliseconds || current_ == total_)
    {
      end_  = now;
      fmt::print("{}{} bytes of {} ({:3.2f}%)",
        prefix,
        current_,
        total_,
        (100.0 * current_) / total_
      );

      auto total_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(now - start_).count();
      if (total_milliseconds > 1000)
      {
        fmt::print(
          " in {:.3f} s ({:.3f} KB/s)",
          total_milliseconds / 1000.0,
          current_ / 1024.0 * 1000.0 / total_milliseconds
        );
      }
    }
  }
};

std::string device_handle_to_str(const tn3::device_handle& dev)
{
  auto ip = dev.ep.address().to_string();
  auto port = dev.ep.port();
  auto version = dev.extra.u32[0];
  return fmt::format(
    "IP: {}, PORT: {}, LUN: {}, VERSION: {}.{:02d}",
    ip, port,
    dev.lun,
    version / 256, version % 256
  );
}

int do_list(const options& opts)
{
  tn3::controller ctl;
  std::vector<tn3::device_handle> tn3devs;
  ctl.enumerate(tn3devs, 200);

  if (tn3devs.size() == 0)
  {
    fmt::print("No TN3 devices found (but it mean nothing)\n");
  }
  else
  {
    for (const auto& dev : tn3devs)
      fmt::print("{}\n", device_handle_to_str(dev));
  }

  return 0;
}

int do_write(const options& opts, tn3::proto::udp::lun_t lun, std::filesystem::path& filepath, bool reverse_bit_order = false)
{
  constexpr std::uint32_t eeprom_image_size = 0x200000;
  constexpr std::uint32_t eeprom_chunk_size = 0x001000;

  static_assert((eeprom_image_size % eeprom_chunk_size) == 0);

  auto image_holder = std::make_unique<char[]>(eeprom_image_size);
  auto image = image_holder.get();

  std::ifstream ifs{ filepath, std::ios::binary | std::ios::ate };

  std::size_t file_image_size = static_cast<std::size_t>(ifs.tellg());

  if (file_image_size > eeprom_image_size)
  {
    fmt::print(
      "file image size {} exceed eeprom size {}\n",
      file_image_size,
      eeprom_image_size);
    return -1;
  }

  ifs.seekg(0);

  std::fill_n(image, eeprom_image_size, 0xFF);

  ifs.read(image, file_image_size);
  if (ifs.bad())
  {
    fmt::print("image file read error\n");
    return -1;
  }

  // align image size
  // file_image_size += (eeprom_chunk_size - 1);
  // file_image_size -= (file_image_size % eeprom_chunk_size);

  if (reverse_bit_order)
    reverse_bit_order_inplace(image, eeprom_image_size);

  tn3::controller ctl;
  std::vector<tn3::device_handle> tn3devs;
  ctl.enumerate(tn3devs, 200);

  tn3::device_handle *pdev = nullptr;
  for (auto& d : tn3devs)
  {
    if (d.lun == lun)
    {
      pdev = &d;
      break;
    }
  }

  if (pdev == nullptr)
  {
    fmt::print("can't find TN3 device with LUN '{}'\n", lun);
    return -1;
  }

  fmt::print("TN3 {}\n", device_handle_to_str(*pdev));

  std::uint32_t offset = 0;
  progress pg_(eeprom_image_size);
  do
  {
    std::size_t transferred;
    auto status = ctl.write_boot_eeprom(*pdev, offset, image + offset, eeprom_chunk_size, transferred);
    if (status != 0 || transferred != eeprom_chunk_size)
    {
      fmt::print("\nwrite eeprom error: 0x{:08x}", status);
      return -1;
    }

    offset += eeprom_chunk_size;
    pg_.advance(eeprom_chunk_size);
    pg_.print("\rwritten ");

  } while (offset < eeprom_image_size);
  fmt::print("\n");

  // verify
  {
    auto chunk_holder = std::make_unique<char[]>(eeprom_chunk_size);
    auto chunk = chunk_holder.get();

    std::uint32_t offset = 0;
    progress pg_(eeprom_image_size);
    do
    {
      std::size_t transferred;
      auto status = ctl.read_boot_eeprom(*pdev, offset, chunk, eeprom_chunk_size, transferred);
      if (status != 0 || transferred != eeprom_chunk_size)
      {
        fmt::print("\nread eeprom error: 0x{:08x}", status);
        return -1;
      }

      if (memcmp(chunk, image + offset, eeprom_chunk_size) != 0)
      {
        fmt::print("\nverify eeprom error at 0x{:08x}", offset);
        return -1;
      }

      offset += eeprom_chunk_size;
      pg_.advance(eeprom_chunk_size);
      pg_.print("\rverified ");
    } while (offset < eeprom_image_size);
    fmt::print("\n");
  }

  return 0;
}

int do_read(const options& opts, tn3::proto::udp::lun_t lun, std::filesystem::path& filepath, bool reverse_bit_order = false)
{
  tn3::controller ctl;
  std::vector<tn3::device_handle> tn3devs;
  ctl.enumerate(tn3devs, 200);

  tn3::device_handle *pdev = nullptr;
  for (auto& d : tn3devs)
  {
    if (d.lun == lun)
    {
      pdev = &d;
      break;
    }
  }

  if (pdev == nullptr)
  {
    fmt::print("can't find TN3 device with LUN '{}'\n", lun);
    return -1;
  }

  fmt::print("TN3 {}\n", device_handle_to_str(*pdev));

  constexpr std::uint32_t eeprom_image_size = 0x200000;
  constexpr std::uint32_t eeprom_chunk_size = 0x001000;

  std::ofstream ofs{ filepath, std::ios::binary };

  static_assert((eeprom_image_size % eeprom_chunk_size) == 0);

  auto buffer_holder = std::make_unique<char[]>(eeprom_chunk_size);
  auto buffer = buffer_holder.get();


  std::uint32_t offset = 0;

  progress pg_(eeprom_image_size);
  do
  {
    std::size_t transferred;
    auto status = ctl.read_boot_eeprom(*pdev, offset, buffer, eeprom_chunk_size, transferred);
    if (status != 0)
    {
      fmt::print("\nread eeprom error: 0x{:08x}", status);
      return -1;
    }

    if (reverse_bit_order)
      reverse_bit_order_inplace(buffer, eeprom_chunk_size);

    ofs.write(buffer, eeprom_chunk_size);
    if (ofs.bad())
    {
      fmt::print("\nfile write error");
      return -1;
    }

    offset += eeprom_chunk_size;
    pg_.advance(eeprom_chunk_size);
    pg_.print("\rread ");

  } while (offset < eeprom_image_size);

  fmt::print("\n");

  return 0;
}

int main(int argc, char *argv[])
{
  std::filesystem::path executable{ argv[0] };
  -- argc;

  auto usage = [&]() -> void
  {
    fmt::print(
      "Usage: {} command [args]\n"
      "  list\n"
      "  read lun outfile\n"
      "  read_rbf lun outfile.rbf\n"
      "  write lun infile\n",
      "  write_rbf lun infile.rbf\n",
      executable.filename().string()
    );
  };

  if (argc < 1)
  {
    usage();
    return 0;
  }

  std::string_view cmd{ argv[1] };
  -- argc;

  auto check_params_count = [&](std::size_t params_count) -> bool
  {
    if (argc == params_count)
      return true;

    fmt::print("command '{}' ", cmd);
    if (params_count == 0)
      fmt::print("doesn't require any parameter\n");
    else
      fmt::print(
        "require exactly {} parameter{}\n",
        params_count,
        params_count == 1 ? "" : "s"
      );

    usage();
    return false;
  };

  options opts;

  if (cmd == "list")
  {
    if (!check_params_count(0))
      return -1;

    return do_list(opts);
  }
  else if (cmd == "read" || cmd == "write" || cmd == "read_rbf" || cmd == "write_rbf")
  {
    if (!check_params_count(2))
      return -1;

    tn3::proto::udp::lun_t lun;
    auto lun_first = argv[2];
    auto lun_last = lun_first + strlen(lun_first);
    auto result = std::from_chars(lun_first, lun_last, lun);

    if ( result.ptr != lun_last
      || result.ec == std::errc::result_out_of_range
      || result.ec == std::errc::invalid_argument)
    {
      fmt::print("can't convert '{}' to lun\n",
        lun_first);
      return -1;
    }
    std::filesystem::path filepath{ argv[3] };

    if (cmd == "read")
      return do_read(opts, lun, filepath);
    else if (cmd == "read_rbf")
      return do_read(opts, lun, filepath, true);
    else if (cmd == "write")
      return do_write(opts, lun, filepath);
    else if (cmd == "write_rbf")
      return do_write(opts, lun, filepath, true);
  }
  else
  {
    fmt::print("unrecognized command '{}'\n", cmd);
    return -1;
  }

  return 0;
}
